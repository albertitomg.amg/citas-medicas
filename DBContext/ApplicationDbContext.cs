﻿using CITAS_MEDICAS.Models;
using Microsoft.EntityFrameworkCore;

namespace CITAS_MEDICAS.DBContext
{
    public class Program
    {
        public static void Main(string[] args)
            => CreateHostBuilder(args).Build().Run();

        // EF Core uses this method at design time to access the DbContext
        public static IHostBuilder CreateHostBuilder(string[] args)
            => Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(
                    webBuilder => webBuilder.UseStartup<Startup>());
    }

    public class Startup
    {
        public void ConfigureServices(IServiceCollection services)
            => services.AddDbContext<ApplicationDbContext>();

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
        }
    }
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
        }

        public DbSet<Usuario> Usuarios { get; set; }
        public DbSet<Paciente> Pacientes { get; set; }
        public DbSet<Medico> Medicos { get; set; }
        public DbSet<Diagnostico> Diagnosticos { get; set; }
        public DbSet<Cita> Citas { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Usuario>().ToTable("Usuarios")
                .HasValue<Usuario>("usuario")
                .HasValue<Paciente>("paciente")
                .HasValue<Medico>("medico")
                .HasValue<Cita>("cita")
                .HasValue<Diagnostico>("diagnostico");

            modelBuilder.Entity<Paciente>().ToTable("Pacientes")
                .HasMany(p => p.medico)
                .HasMany(m => m.paciente)
                .HasForeignKey(m => m.NSS);

            modelBuilder.Entity<Medico>().ToTable("Medicos")

            modelBuilder.Entity<Cita>().ToTable("Citas")

            modelBuilder.Entity<Diagnóstico>().ToTable("Diagnóstico")
        }

        public class Usuario
        {
            public string nombre { get; set; }
            public string apellidos { get; set; }
            public string usuario { get; set; }
            public string clave { get; set; }
        }

        public class Paciente : Usuario
        {
            public string NSS { get; set; }
            public string numTarjeta { get; set; }
            public string telefono { get; set; }
            public string direccion { get; set; }
        }
        public class Medico : Usuario
        {
            public string numColegiado { get; set; }
        }
        public class Cita : Usuario
        {
            public DateTime fechaHora { get; set;}
            public string motivoCita { get;}
            public int attrib11 { get; set;}
        }
        public class Diagnostico : Usuario
        {
            public string valoracionEspecialista { get; set; }
            public string enfermedad { get; set; }
        }
    }
}